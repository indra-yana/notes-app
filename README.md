## NotesApp

Simple Notes App

## Features
- Java
- Room Database

## Notes
- Build on android studio 4.0

## Screenshots
<p align="center">
    <img src="screenshot/home.png" width="250" title="Click to enlarge">
    <img src="screenshot/home-list.png" width="250" title="Click to enlarge">
    <img src="screenshot/home-search-note.png" width="250" title="Click to enlarge">
</p>

<p align="center">
    <img src="screenshot/add-note-main.png" width="250" title="Click to enlarge">
    <img src="screenshot/add-note-url.png" width="250" title="Click to enlarge">
    <img src="screenshot/add-note-image.png" width="250" title="Click to enlarge">
</p>

